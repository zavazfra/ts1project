import org.junit.Assert;
import org.junit.Test;

public class FaktorialTest {

	@Test
	public void name() {
		Faktorial f = new Faktorial();
		Assert.assertEquals(f.factorial(3), 6);
		Assert.assertEquals(f.factorial(4), 24);
		Assert.assertEquals(f.factorial(0),-1);
	}
}
