public class Faktorial {

	public long factorial(long number) {
		if(number == 1) {
			return 1;
		}
		if(number < 1) {
			return 1;
		}
		return number*this.factorial(number-1);
	}

}
